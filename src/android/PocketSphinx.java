package cordova-plugin-pocket-sphinx;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class PocketSphinx extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("parseAudio")) {
            String message = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    this.parseAudio(message, callbackContext);
                    return true;
                }
            });
        }
        return false;
    }

    private void parseAudio(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
