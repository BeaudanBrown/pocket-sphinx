var exec = require('cordova/exec');

exports.parseAudio = function(arg0, success, error) {
    exec(success, error, "PocketSphinx", "parseAudio", [arg0]);
};
